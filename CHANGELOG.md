<!--- BEGIN HEADER -->
# Changelog

All notable changes to this project will be documented in this file.
<!--- END HEADER -->

## [0.1.4](https://gitlab.com/paquetes-bittacora/bpanel-4/compare/v0.1.3...v0.1.4) (2022-07-28)

### Bug Fixes

* Update Bpanel4Installer.php ([9cf279](https://gitlab.com/paquetes-bittacora/bpanel-4/commit/9cf279f10bc72121668df0b5d995ead2f7a8ca87))


---

## [0.1.3](https://gitlab.com/paquetes-bittacora/bpanel-4/compare/v0.1.2...v0.1.3) (2022-07-28)

### Bug Fixes

* Bpanel4Installer.php ([da9273](https://gitlab.com/paquetes-bittacora/bpanel-4/commit/da9273d5cf476ae1649406f3f0271c880c65cede))


---

## [0.1.2](https://gitlab.com/paquetes-bittacora/bpanel-4/compare/v0.1.1...v0.1.2) (2022-07-28)

### Bug Fixes

* Añadir storage:link a las instrucciones de instalación ([4a9878](https://gitlab.com/paquetes-bittacora/bpanel-4/commit/4a987807c484f7ce0c7e051d4847bc08b6704759))
* Corregir texto ([49fcab](https://gitlab.com/paquetes-bittacora/bpanel-4/commit/49fcab9b2966b0b9bcd691b186b73efa6f0daf24))


---

## [0.1.1](https://gitlab.com/paquetes-bittacora/bpanel-4/compare/v0.1.0...v0.1.1) (2022-07-28)

### Bug Fixes

* Actualizar Bpanel4Installer.php ([0b4f7b](https://gitlab.com/paquetes-bittacora/bpanel-4/commit/0b4f7bb1a8b290ca4a2314d26566949334b522fd))


---

## [0.1.0](https://gitlab.com/paquetes-bittacora/bpanel-4/compare/v0.0.1...v0.1.0) (2022-07-28)

### Features

* Actualizar instalador para que use vite en vez de webpack ([3769c3](https://gitlab.com/paquetes-bittacora/bpanel-4/commit/3769c3d573c974c918d679942af11a6412552fb3))


---

## [0.0.1](https://gitlab.com/paquetes-bittacora/bpanel-4/compare/565607e3764ef7262919b440fd218f73b1b391e6...v0.0.1) (2022-07-27)

### Features

* Añadir autoinstalador ([b18e76](https://gitlab.com/paquetes-bittacora/bpanel-4/commit/b18e76c81160c1404d3547c6099485bdba7b3e98))

### Bug Fixes

* Corregir readme ([105fb1](https://gitlab.com/paquetes-bittacora/bpanel-4/commit/105fb10e582eb360db82dfc1109346c2ce914676))


---

