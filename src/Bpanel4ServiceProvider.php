<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4;

use Bittacora\Bpanel4\Commands\Bpanel4Installer;
use Illuminate\Support\ServiceProvider;

class Bpanel4ServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->commands([Bpanel4Installer::class]);

        $this->publishes([
            __DIR__ . '/../config/bpanel4.php' => config_path('bpanel4.php'),
        ], 'bpanel4');
    }
}
