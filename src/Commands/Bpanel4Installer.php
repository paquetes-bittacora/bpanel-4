<?php

namespace Bittacora\Bpanel4\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Bpanel4Installer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bp4:install';

    protected $description = 'Instala bPanel4 en Laravel';

public function handle(): void
    {
        $this->info("Instalando bPanel4. Recuerda que debes configurar tu .env para que el comando funcione.\n");

        $this->info("Ejecutando las migraciones. Se volverán a ejecutar durante la instalación de bPanel4.");
        Artisan::call('migrate');

        $webpackMix = file_get_contents(base_path('vite.config.js'));

        $this->info('Actualizando vite.config.js');

        // Paso 1
//        $search = "mix.js('resources/js/app.js', 'public/js')";
//        $webpackMix = str_replace(
//            $search,
//            "mix.copyDirectory('resources/bpanel4/assets', 'public/assets'); // Añadido por bPanel4\n\n" . $search,
//            $webpackMix
//        );



        // Paso 2
        // El formato en esta parte del código está "mal" para que al escribirse a webpack.mix.js quede bien.
        // Las líneas ~@bp4Start@~ y ~@bp4End@~ sirven para facilitar añadir líneas a este archivo desde otros paquetes
        // sin tener que usar expresiones regulares.

        $search = "import laravel from 'laravel-vite-plugin';";
        $webpackMix = str_replace($search, $search."\nconst path = require('node:path');", $webpackMix."\n");

        $search = 'export default defineConfig({';
        $webpackMix = str_replace($search, $search."\n"."resolve: {
        alias: {
            '~font-awesome/css/font-awesome.min.css': path.resolve(__dirname, 'node_modules/font-awesome/css/font-awesome.min.css'),
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
            '~select2': path.resolve(__dirname, 'node_modules/select2'),
            '~font-awesome': path.resolve(__dirname, 'resources/bpanel4/assets/vendor/fontawesome-pro-5.15.4'),
            '~datatables.net-bs4': path.resolve(__dirname, 'node_modules/datatables.net-bs4'),
            '~datatables.net-buttons-bs4': path.resolve(__dirname, 'node_modules/datatables.net-buttons-bs4'),
            '~datatables.net-rowreorder-bs4': path.resolve(__dirname, 'node_modules/datatables.net-rowreorder-bs4')
        }
    },", $webpackMix);

        $search = "input: ['resources/css/app.css', ";
        $add = "\n    // ~@bp4Start@~
        'node_modules/jquery/dist/jquery.js',
        'node_modules/flatpickr/dist/flatpickr.css',
        'node_modules/tinymce/tinymce.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'vendor/bittacora/bpanel4-panel/resources/assets/js/ace.js',
        'resources/bpanel4/assets/sass/app.scss',
        'resources/bpanel4/assets/scss/ace.scss',
        'resources/bpanel4/assets/scss/login.scss',
        'vendor/bittacora/bpanel4-panel/resources/assets/js/app.js',
        'resources/bpanel4/assets/js/app.public.js',
        'vendor/bittacora/bpanel4-panel/resources/assets/js/livewire-sortable.js',
        'resources/bpanel4/assets/vendor/fontawesome-pro-5.15.4/css/all.css',
        '/node_modules/flatpickr/dist/flatpickr.js',
        '/node_modules/flatpickr/dist/flatpickr.css',
        '/node_modules/tinymce/skins/content/default/content.css',
        'vendor/bittacora/bpanel4-panel/resources/assets/js/ace.js',
   // ~@bp4End@~\n";
        $webpackMix = str_replace($search, $search . "\n".$add, $webpackMix);
        file_put_contents(base_path('vite.config.js'), $webpackMix);

        // Paso 3
        $this->info('Actualizando resources/js/app.js');
        $appJsPath = base_path('resources/js/app.js');
        $originalContent = file_get_contents($appJsPath);
        $newContent = $originalContent . "\n\n// bPanel4\nwindow.$ = window.jQuery = import('jquery');

// Flatpickr
import flatpickr from 'flatpickr';
import '/node_modules/flatpickr/dist/flatpickr.css';\n";
        file_put_contents($appJsPath, $newContent);

        // Paso 4
        $this->info("Publicando paquetes");
        Artisan::call("vendor:publish --tag=fortify-support --force");
        Artisan::call("vendor:publish --tag=fortify-config --force");
        Artisan::call("vendor:publish --tag=bpanel4-panel --force");
        Artisan::call("vendor:publish --tag=datatables-buttons --force");
        Artisan::call('vendor:publish --tag=livewire-tables-config --force');
        Artisan::call("vendor:publish --tag=lang");
        Artisan::call("vendor:publish --tag=bpanel4");
        Artisan::call("fortify-translations:install");

        \File::copyDirectory( base_path('resources/bpanel4/assets'), base_path('public/assets'));


    // Paso 5
        $this->info("Ejecutando las migraciones");
        Artisan::call("migrate --seed");

        // Paso 6
        $this->info("Actualizando configuración de Livewire tables");
        $path = base_path('config/livewire-tables.php');
        $originalContent = file_get_contents($path);
        $newContent = str_replace("tailwind", "bootstrap-4", $originalContent);
        file_put_contents($path, $newContent);
        
        $this->line("");
        $this->updateConfigFile();

        $this->line("");
        $this->info("Ejecuta los siguientes comandos para completar la instalación de bPanel 4:\n");
        $this->line("npm install --legacy-peer-deps --save jquery alpinejs bootstrap@^4.0.0 datatables.net-bs4 datatables.net-bs4@^1.10.24 datatables.net-buttons-bs4 datatables.net-buttons-bs4@^1.7.0 datatables.net-editor datatables.net-editor@^2.0.2 datatables.net-rowreorder-bs4 datatables.net-rowreorder-bs4@^1.2.7 flatpickr font-awesome font-awesome@^4.7.0 install path popper.js resolve-url-loader@^5.0.0 sass sass-loader@^12.1.0 select2@^4.1.0-rc.0 sweetalert2 tinymce tinymce-i18n bootstrap-fileinput bootstrap-duallistbox\n
        npm npm run dev\n\n");
        $this->info("Después solo tendrás que ejecutar php artisan serve y ya tendrás bPanel 4 funcionando");
    }
    
    private function updateConfigFile(): void
    {
        $adminMail = $this->ask('Introduce tu email o el email al que quieras que lleguen los correos de administración');
        $contents = file_get_contents(config_path('bpanel4.php'));
        $newContents = str_replace('__ADMIN__EMAIL__', $adminMail, $contents);
        file_put_contents(config_path('bpanel4.php'), $newContents);
    }
}
