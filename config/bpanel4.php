<?php

declare(strict_types=1);

return [
    'admin_email' => '__ADMIN__EMAIL__',
];
