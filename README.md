<div align="center">
    ![Logo](resources/images/logo-bpanel.png)
</div>

Paquete para instalar bPanel 4 en Laravel.

## Requisitos

Hay que usar una versión de Laravel igual o posterior a la 9.36.11, que es posterior al cambio de **Webpack** por **Vite**, y hubo que cambiar cosas de los templates, instalación etc. Aunque vite se introdujo en la 9.1, posteriormente añadieron algunas funciones que usamos en algunos templates. 

Habrá que usar también PHP 8.1 o superior y versiones actuales de Node y NPM (que no son requisitos de bPanel 4 sino del propio Laravel).

## Instalación

Son muchos pasos pero son sencillos:

1. Creamos un proyecto nuevo con Laravel: `composer create-project laravel/laravel`
2. Configurar `.env` (configurar la conexión a la base de datos que hemos creado para el proyecto)
3. Establecer locale en `app/config.php`
4. Añadir repositorios al `composer.json` de la raíz del proyecto.
```
    "repositories": [
      {
        "type": "composer",
        "url": "https://composer.bittacora.dev"
      },
      {
        "type": "vcs",
        "url": "https://github.com/laravel-shift/uniquewith-validator.git"
      }
    ]
```
*El repositorio de Laravel Shift es necesario porque una de las dependencias del proyecto no está actualizada a PHP 8, y han hecho un fork para actualizarla*

5. Añadir `"@php artisan bpanel4:install-packages"` al apartado `"post-autoload-dump"` de `composer.json`
6. Ejecutar `composer require bittacora/bpanel4`
7. Ejecutar `php artisan bp4:install`
8. Ejecutar los comandos que indica el comando anterior
9. `php artisan serve`

# Instalar paquetes

bPanel4 está preparado para que sus paquetes se instalen automáticamente. Solo habrá que instalarlos por composer, y cada paquete ejecutará los pasos necesarios para instalarse automáticamente.
